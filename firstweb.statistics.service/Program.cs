﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using GemBox.Spreadsheet;
using Newtonsoft.Json;

namespace ConsoleApplication1
{
    class Program
    {
        private const string serverurl =
            "E:\\Dynamicweb\\SolutionsCustom\\adformstats.1stweb-test.net\\Application\\files";

        private const string localUrl = "c:\\files";
        static void Main(string[] args)
        {
            Console.WriteLine(DateTime.Now.ToShortTimeString() + " Started adform listening service.." + "\n");
            WatchFolder();
            Console.ReadKey();
        }
        public static void WatchFolder()
        {
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = Path.GetFullPath(serverurl);
            watcher.NotifyFilter = NotifyFilters.Size;
            watcher.Filter = "*";
            watcher.Changed += Onchange;
            watcher.EnableRaisingEvents = true;

        }

        public static void Onchange(object src, FileSystemEventArgs e)
        {
            FileInfo file = new FileInfo(e.FullPath);
            Console.WriteLine(file.Length);
            if (file.Length > 1000)
            {
                Console.Write(DateTime.Now.ToShortTimeString() + " File found: " + file.Name + "\n");
                if (file.Directory != null)
                    RetrieveData(file.Name);
            }
        }

        public static void RetrieveData(string filePlacement)
        {
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");

            var reportData = new Report();
            var filePL = serverurl + "\\" + filePlacement;
            ExcelFile ef = ExcelFile.Load(filePL);
            var sheets = ef.Worksheets.ActiveWorksheet;

            foreach (var column in sheets.Rows)
            {
                string strategyItem = "";
                if (column.Cells[5].Value != null)
                {

                    strategyItem = column.Cells[5].Value.ToString();
                    reportData.RtbName = strategyItem;
                }

                string campaignId = "";
                if (column.Cells[6].Value != null)
                {
                    campaignId = column.Cells[6].Value.ToString();
                    int convertedId = -1;
                    int.TryParse(campaignId, out convertedId);
                    if (convertedId != -1)
                    {
                        reportData.CampaignId = convertedId;
                    }

                }

                string placementId = "";
                if (column.Cells[11].Value != null)
                {
                    placementId = column.Cells[11].Value.ToString();
                    int convertedId = -1;
                    int.TryParse(placementId, out convertedId);
                    if (convertedId != -1)
                    {
                        reportData.PlacementId = convertedId;
                    }

                }
                string cost = "";
                if (column.Cells[18].Value != null)
                {
                    cost = column.Cells[18].Value.ToString();
                    int convertedClick = -1;
                    int.TryParse(cost, out convertedClick);
                    if (convertedClick != -1)
                    {
                        reportData.Cost = convertedClick;
                    }
                }
                string impression = "";
                if (column.Cells[20].Value != null)
                {
                    impression = column.Cells[20].Value.ToString();
                    int convertedImpression = -1;
                    int.TryParse(impression, out convertedImpression);
                    if (convertedImpression != -1)
                    {
                        reportData.Impressions = convertedImpression;
                    }
                }

                string clicks = "";
                if (column.Cells[23].Value != null)
                {
                    clicks = column.Cells[23].Value.ToString();
                    int convertedBounce = -1;
                    int.TryParse(clicks, out convertedBounce);
                    if (convertedBounce != -1)
                    {
                        reportData.Clicks = convertedBounce;
                    }
                }

                string conversions = "";
                if (column.Cells[27].Value != null)
                {
                    conversions = column.Cells[27].Value.ToString();
                    int convertedPw = -1;
                    int.TryParse(conversions, out convertedPw);
                    if (convertedPw != -1)
                    {
                        reportData.Conversions = convertedPw;
                    }
                }

                string ecpm = "";
                if (column.Cells[30].Value != null)
                {
                    ecpm = column.Cells[30].Value.ToString();
                    double convertedCost = -1;
                    double.TryParse(ecpm, out convertedCost);
                    if (convertedCost != -1)
                    {
                        reportData.Ecpm = convertedCost;
                    }
                }

                string ecpc = "";
                if (column.Cells[31].Value != null)
                {
                    ecpc = column.Cells[31].Value.ToString();
                
                    double convertedWinRate = -1;
                    double.TryParse(ecpc, out convertedWinRate);
                    if (convertedWinRate != -1)
                    {
                        reportData.Ecpc = convertedWinRate;
                    }
                }

                string ecpa = "";
                if (column.Cells[34].Value != null)
                {
                    ecpa = column.Cells[34].Value.ToString();
                    int convertedVisitors = -1;
                    int.TryParse(ecpa, out convertedVisitors);
                    if (convertedVisitors != -1)
                    {
                        reportData.Ecpa = convertedVisitors;
                    }
                }

                string CtrUnique = "";
                if (column.Cells[35].Value != null)
                {
                    CtrUnique = column.Cells[35].Value.ToString();
                    if (CtrUnique.Contains("%"))
                    {
                        CtrUnique.Replace("%", "");
                    }
                    double convertedCtrUnique = -1;
                    double.TryParse(CtrUnique, out convertedCtrUnique);
                    if (convertedCtrUnique != -1)
                    {
                        reportData.CtrUnique = convertedCtrUnique;
                    }
                }
                string cov = "";
                if (column.Cells[37].Value != null)
                {

                    cov = column.Cells[37].Value.ToString();
                    if (CtrUnique.Contains("%"))
                    {
                        cov.Replace("%", "");
                    }
                    double convertedCtrUnique = -1;
                    double.TryParse(cov, out convertedCtrUnique);
                    if (convertedCtrUnique != -1)
                    {
                        reportData.Cov = convertedCtrUnique;
                    }

                }
                string sales = "";
                if (column.Cells[39].Value != null)
                {
                    sales = column.Cells[39].Value.ToString();
                    double convertedecpc = -1;
                    double.TryParse(sales, out convertedecpc);
                    if (convertedecpc != -1)
                    {
                        reportData.ProductSales = convertedecpc;
                    }
                }

                string productCount = "";
                if (column.Cells[40].Value != null)
                {
                    productCount = column.Cells[40].Value.ToString();
                    int convertedecpp = -1;
                    int.TryParse(productCount, out convertedecpp);
                    if (convertedecpp != -1)
                    {
                        reportData.Productcount = convertedecpp;
                    }
                }

  
                if (reportData.PlacementId != 0 && reportData.CampaignId != -1 && reportData.CampaignId != 0 && !string.IsNullOrEmpty(reportData.RtbName) && reportData.Clicks != -1 && reportData.Impressions != -1)
                {
                    if (reportData.RtbName != "TOTAL")
                    {
                        var matchingReport = GetReports().Where(r => r.PlacementId == reportData.PlacementId);
                        if (matchingReport.Any())
                        {

                            var report = matchingReport.First();

                            report.Clicks = reportData.Clicks;
                            report.Impressions = reportData.Impressions;
                            report.Cost = reportData.Cost;
                            report.Bounce = reportData.Bounce;
                            report.PageViews = reportData.PageViews;
                            report.Cost = reportData.Cost;
                            report.ProductSales = reportData.ProductSales;
                            report.CtrUnique = reportData.CtrUnique;
                            report.Ecpa = reportData.Ecpa;
                            report.Ecpc = reportData.Ecpc;
                            report.Cov = reportData.Cov;
                            report.Ecpm = reportData.Ecpm;
                            report.Conversions = reportData.Conversions;
                            PutReport(report.Id, report);
                            Console.WriteLine(DateTime.Now.ToShortTimeString() + " Updated " + report.RtbName);
                        }
                        else
                        {
                            reportData.RtbName = reportData.RtbName.Replace("Real Time Bidding ::", "");
                            PostReport(reportData);
                            Console.WriteLine(DateTime.Now.ToShortTimeString() + " Added Data for line item: " + reportData.RtbName + "\n");
                        }
                    }
                }


            }
            Console.WriteLine(DateTime.Now.ToShortTimeString() + " All data fetched!" + "\n");


        }

        public static IEnumerable<Report> GetReports()
        {
            var requestJson = Get("http://adformstats.1stweb-test.net/api/reports");
            var root = JsonConvert.DeserializeObject<List<Report>>(requestJson);
            return root;
        }

        public static Report PostReport(Report report)
        {

            var post = Post("http://adformstats.1stweb-test.net/api/reports", report);
            var root = JsonConvert.DeserializeObject<Report>(post);

            return root;
        }

        public static Report PutReport(int id, Report report)
        {

            var requestJson = Put("http://adformstats.1stweb-test.net/api/reports/" + id, report);
            var root = JsonConvert.DeserializeObject<Report>(requestJson);
            return root;
        }
        public static string Get(string link)
        {
            try
            {

                var req = WebRequest.Create(link);
                req.Method = "GET";
                req.ContentType = "Application/json";

                var response = (HttpWebResponse)req.GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new InvalidOperationException("Unexpected response: " + response.StatusCode);
                }
                string json;

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                return json;



            }
            catch (Exception)
            {

                throw;
            }
        }

        public static string Post(string link, object postData)
        {
            var request = WebRequest.Create(link);
            request.Method = "POST";
            request.ContentType = "Application/json";

            var json = JsonConvert.SerializeObject(postData);

            request.ContentLength = Encoding.UTF8.GetByteCount(json);

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
            }

            var response = (HttpWebResponse)request.GetResponse();

            //if (response.StatusCode != HttpStatusCode.OK || response.StatusCode != HttpStatusCode.NoContent)
            //{
            //    throw new InvalidOperationException("Unexpected response: " + response.StatusCode);
            //}

            using (var reader = new StreamReader(response.GetResponseStream()))
            {
                json = reader.ReadToEnd();
            }

            return json;

        }
        public static string Put(string link, object postData)
        {
            var request = WebRequest.Create(link);
            request.Method = "PUT";
            request.ContentType = "Application/json";

            var json = JsonConvert.SerializeObject(postData);

            request.ContentLength = Encoding.UTF8.GetByteCount(json);

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
            }

            var response = (HttpWebResponse)request.GetResponse();

            //if (response.StatusCode != HttpStatusCode.OK || response.StatusCode != HttpStatusCode.NoContent)
            //{
            //    throw new InvalidOperationException("Unexpected response: " + response.StatusCode);
            //}

            using (var reader = new StreamReader(response.GetResponseStream()))
            {
                json = reader.ReadToEnd();
            }

            return json;
        }

        public class Report
        {
            public int Id { get; set; }
            public int CampaignId { get; set; }
            public int PlacementId { get; set; }
            public string RtbName { get; set; }
            public int Clicks { get; set; }
            public int Impressions { get; set; }
            public int Bounce { get; set; }
            public int PageViews { get; set; }
            public double Cost { get; set; }
            public int Conversions { get; set; }
            public double ProductSales { get; set; }
            public int Productcount { get; set; }
            public double CtrUnique { get; set; }
            public double Ecpa { get; set; }
            public double Ecpc { get; set; }
            public double Ecpm { get; set; }
            public double Cov { get; set; }
        }


    }
}
